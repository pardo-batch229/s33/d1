// console.log("Hello world");

//[SECTION] - Synchronous vs Asynchronous
// JS - Synchronous 
// It reads from top to bottom, left to right
// Synchronous stops when it meets an error within the line of code while Asynchronous can reads all the line even if there's an error within the code block

console.log("Hello world");
// onsole.log("Hello again");
console.log("Goodbye");

// [SECTION] - Getting all post
// The Fetch API allows you to asynchronously request for a resource (data)
		// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value
		// Syntax
			// fetch('URL')
// checking if the connection or the server/url is still available or working
console.log(fetch("https://jsonplaceholder.typicode.com/posts").then(response => console.log(response.status)));

fetch("https://jsonplaceholder.typicode.com/posts").then((response) => response.json())// use the json method from the response object to convert data retrieved into JSON Format.
.then((json) => console.log(json));

// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
		// Used in functions to indicate which portions of code should be waited for
		// Creates an asynchronous function

async function fetchData(){
    // waits for the fetch method to complete then stores the value in the result variable
    let result = await fetch("https://jsonplaceholder.typicode.com/posts")

    console.log(result);
    console.log(typeof result);

    // Converts the data from the response to JSON 
    let json = await result.json();
    console.log(json);
}
fetchData();

// [SECTION] - Getting a specific post
// Wildcard
// Retrieves a specific post following the res api (retrieves, /posts:id, GET)
fetch("https://jsonplaceholder.typicode.com/posts/1")
.then((response)=> response.json())
.then((json) => console.log(json));

// [SECTION] - Creating a Post
fetch("https://jsonplaceholder.typicode.com/posts",{
    method: "POST",
    headers: {
        "Content-Type" : "application/json"
    },
    body: JSON.stringify({
        title: "New Post",
        body: "Hello World",
        userId: 1
    })
})
.then((response)=> response.json())
.then((json) => console.log(json));

// [SECTION] - Updating a Post
fetch("https://jsonplaceholder.typicode.com/posts/1",{
    method: "PUT",
    headers: {
        "Content-Type" : "application/json"
    },
    body: JSON.stringify({
        id: 1,
        title: "Update Post",
        body: "Hello World",
        userId: 1
    })
})
.then((response)=> response.json())
.then((json) => console.log(json));

// [SECTION] - Updating a Post using PATCH
fetch("https://jsonplaceholder.typicode.com/posts/1",{
    method: "PATCH",
    headers: {
        "Content-Type" : "application/json"
    },
    body: JSON.stringify({
        title: "Corrected Post using PATCH",
    })
})
.then((response)=> response.json())
.then((json) => console.log(json));

// [SECTION] - Deleting a post
fetch("https://jsonplaceholder.typicode.com/posts/1",{
    method: "DELETE"
});

// [SECTION] - Filtering Post
fetch("https://jsonplaceholder.typicode.com/posts?userId=10")
.then((response) => response.json())
.then((json) => console.log(json))

//WILDCARD
// /post/:id 